package com.example.krina.intentbottomsheetpicker;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.flipboard.bottomsheet.BottomSheetLayout;
import com.flipboard.bottomsheet.commons.IntentPickerSheetView;

public class MainActivity extends AppCompatActivity {

    BottomSheetLayout bottomSheetLayout;
    Button customview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bottomSheetLayout = (BottomSheetLayout) findViewById(R.id.bottomsheet);
        customview = (Button)findViewById(R.id.btn_custom_view);
        customview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Invitation");
                shareIntent.putExtra(Intent.EXTRA_TEXT, "http://www.eswasthya.in/android");

                IntentPickerSheetView intentPickerSheetView = new
                        IntentPickerSheetView(MainActivity.this, shareIntent, "Invite your Friend",
                        new IntentPickerSheetView.OnIntentPickedListener() {
                    @Override
                    public void onIntentPicked(IntentPickerSheetView.ActivityInfo activityInfo) {
                        bottomSheetLayout.dismissSheet();
                        startActivity(activityInfo.getConcreteIntent(shareIntent));
                    }
                });

                bottomSheetLayout.showWithSheetView(intentPickerSheetView);


            }
        });


    }
}
